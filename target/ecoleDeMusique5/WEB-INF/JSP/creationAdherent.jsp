<%@include file="taglibs.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Insert title here</title>
		<link rel="stylesheet" type="text/css" href="bootstrap.min.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/spacelab/bootstrap.min.css" media="all"/>
	</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="container">		
	
		<%-- 	${applicationScope.variable }	 --%>	
		<h1 class="text-center m- p-2">Cr�ation Adh�rent</h1><br>		
		<div class="alert alert-dismissible alert-${classCss}">
	 		 <button type="button" class="close" data-dismiss="alert">&times;</button>
	  		 <a href="#" class="alert-link">${message}</a> 
		</div>		
		 <br><br>	
		 	
		<h2 class="m-2">CREATION D'UN ADHERENT:</h2>		
		<form method="post" action="#" >
		  <div class="form-group">
		    <label for="nom">Nom</label>
		    <input value="${personne.nom}"  type="text" class="form-control" id="nom" name="nom" aria-describedby="">
		    <small id="emailHelp" class="form-text text-muted">Nous ne partagerons jamais votre nom avec un tiers.</small>
		  </div>
		  <div class="form-group">
		    <label for="prenom">Prenom</label>
		    <input value="${personne.prenom}" type="text" class="form-control" id="prenom" name="prenom">
		  </div>
		  <input type="submit" value="Valider" > 
		</form>	
			
		<%@ include file="footer.jsp" %>
	</div>
</body>
</html>