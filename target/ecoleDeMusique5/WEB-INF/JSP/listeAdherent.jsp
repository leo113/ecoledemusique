<%@include file="taglibs.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/spacelab/bootstrap.min.css" media="all"/>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="container">
	
		<h1 class="text-center m-2 p-2">Liste Des Adh�rents</h1>
		
		<!-- CALCUL DU NBRE D ADHERENT DANS LA LISTE -->
		<!-- applicationScope.listAdherents.listPersonne -->
		<c:set var="nbrAdherent" value="${empty requestScope.listPersonnes ?
			 'AUCUN ADHERENT' : fn:length(requestScope.listPersonnes)}" /> 
			
		<h3 class="text-center m-2 p-2">
			Nombre d'adh�rents : <c:out value="${nbrAdherent }"></c:out>
		</h3>
		
		<!-- CREATION DE LA TABLE DES ADHERENTS -->
		<c:if test="${fn:length(requestScope.listPersonnes) gt 0}">
			<table class="table">
				  <thead>
				    <tr>
				      <th scope="col">Identifiant</th>
				      <th scope="col">Nom</th>
				      <th scope="col">Pr�nom</th>
				    </tr>
				  </thead>
				  <tbody>
					<c:forEach items="${requestScope.listPersonnes}" var="map">
						<tr>
							 <td>${map.id}</td>
							 <td>${map.nom}</td>
							 <td>${map.prenom}</td>
						 </tr>
					</c:forEach>			  
				  </tbody>
			</table>
		</c:if>
		
		<!-- FORWARD -->
		<%-- <jsp:forward page="/accueil"/> --%>
		
		<%@ include file="footer.jsp" %>
	</div>
</body>
</html>