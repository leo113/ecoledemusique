<%@include file="taglibs.jsp" %>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="accueil">Ecole De Musique</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
      <li>
        <a class="nav-link" href="accueil">Accueil <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="listeAdherent">Liste Adh�rents</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="creationAdherent">Cr�ation</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="modificationAdherent">Modification</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="suppressionAdherent">Suppression</a>
      </li>
    </ul>
<!--     <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form> -->
  </div>
</nav>