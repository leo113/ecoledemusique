<%@include file="taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Accueil</title>
<!-- 	<style>
		.imageDeFond {
		  background: url(./asset/images/musique.jpg);
		  background-size: 100px 80px;
		  background-repeat: no-repeat;
		}	
	</style> -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/spacelab/bootstrap.min.css" media="all"/>
	<!--  --><link rel="stylesheet" type="text/css" href="../asset/css/main.css" media="all"/>
</head>
<body>	
	<%@ include file="header.jsp" %>
	<div class="container">	
		
		<div class="jumbotron bg-secondary mt-5 imageDeFond disabled">
		  <h1 class="display-3 text-center">Ecole de musique <br> Accueil des adh�rents</h1>
		  <p class="lead  text-white">Cours de musique et de chant � domicile ou en visio.</p>
		  <hr class="my-4">
		   <p class="lead">
		    <a class="btn btn-secondary" href="#" role="button">En savoir plus</a>
		  </p>
		  
		  <br> 
		  <!-- REDIRECTION -->
		  <%-- <c:redirect url="modificationAdherent"/> --%>
		  
		  <!--  FORWARD -->
		  <%-- <jsp:forward page="/accueil"/> --%>
		  
		</div>	
		
		<div class="text-center">
		<h3 >INSCRIPTIONS CES DERNIERES 24H</h3>
		  <table class="table">
				  <thead>
				    <tr>
				      <th scope="col">Nom: </th>
				    </tr>
				  </thead>
				  <tbody>
		  			<c:forEach items="${requestScope.cokies}" var="map">
						<tr>
							 <td>${map.value}</td>
						 </tr>
					</c:forEach>			  
				  </tbody>
			</table>
		 </div>
		  
		 <a href="?init=init" class="btn btn-primary">R�initialiser le compteur de pages</a>
		  	
	</div>
	
	<%@ include file="footer.jsp" %>
</body>
</html>