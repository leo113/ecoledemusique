<%@include file="taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="bootstrap.min.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/spacelab/bootstrap.min.css" media="all"/>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="container">
	
		<h1 class="text-center m-2 p-2">Modification Adh�rent  </h1>
		
		<!-- MESSAGE DE ERREUR/SUCCESS -->
	 	<div class="alert alert-dismissible alert-${classCss}">
	 		 <button type="button" class="close" data-dismiss="alert">&times;</button>
	  		 <a href="#" class="alert-link">${message}</a> 
		</div><br>	
 		 
 		 <!-- FORMULAIRE DE SELECTION D'UN ADHERENT -->
		 <form method="post" action="#" >
		  <div class="form-group">
		    <label for="exampleFormControlSelect1" class="font-weight-bold font-italic">S�lectionnez l'adh�rent � modifer : </label>
		    <select class="form-control" id="exampleFormControlSelect1" name="adherentAModifier">
		    	<c:forEach items="${listePersonne}" var="news">
		     		 <option > <c:out value="${news['nom']}"></c:out> </option>		     
		        </c:forEach>
		    </select>
		  </div>
			  <input type="submit" value="Valider" > 
		</form> <br><br><br>
		
		<!-- FORMULAIRE DE MODIFICATION/CREATION D'UN ADHERENT -->
		<h2 class="m-2">MOFIFIER L'ADHERENT :</h2>
		<form method="post" action="#" >
		  <div class="form-group">
		    <label for="nom">Nom</label>
		    <input ${mode}  value="${personne.nom}"  type="text" class="form-control" id="nom" name="nom" aria-describedby="">
		    <small id="emailHelp" class="form-text text-muted">Nous ne partagerons jamais votre nom avec un tiers.</small>
		  </div>
		  <div class="form-group">
		    <label  for="prenom">Prenom</label>
		    <input  ${mode} value="${personne.prenom}" type="text" class="form-control" id="prenom" name="prenom">
		  </div>
		  <div class="form-group">
		    <input value="${personne.id}" type="hidden" class="form-control" id="id" name="id">
		  </div>
		  <input type="submit" value="Valider"> 
		</form>
	
		<%@ include file="footer.jsp" %>
	</div>
</body>
</html>