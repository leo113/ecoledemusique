package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import Controller.CreationAdherentController;
import Controller.ICommand;
import Controller.ListeAdherentController;
import Controller.ModificationAdherentController;
import Controller.PageAccueilController;
import Controller.SuppressionAdherentController;
import Exceptions.ExceptionDao;
import metier.ListeAdherents;
import modele.beans.Personne;
import modele.dao.DaoPersonne;

@WebServlet (name = "FrontController", urlPatterns = {"/"})
public class FrontController extends HttpServlet {
	
	// ATTRIBUTS STATIC
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/ecole_musique") public static DataSource dataSource;
	private static Connection connection;
	
	// ATTRIBUTS D INSTANCE
	private Map<String, ICommand> commands=new HashMap<>();

	// CONSTRUCTEUR
	/**
     * @see HttpServlet#HttpServlet()
     */
    public FrontController() {
        super();
    }
    
    // GETTERS ET SETTERS
	public static Connection getConnection() {
		return connection;
	}
	
    public void init() throws ServletException {
    	
    	try {
			connection = dataSource.getConnection();
		} catch (SQLException e1) {
			throw new ServletException(e1.getMessage());
		}
    	commands.put("listeAdherent", new ListeAdherentController() ) ;
    	commands.put("accueil", new PageAccueilController()) ;
    	commands.put("creationAdherent", new CreationAdherentController() ) ;
    	commands.put("modificationAdherent", new ModificationAdherentController() ) ;
    	commands.put("suppressionAdherent", new SuppressionAdherentController() ) ;    	
    	
    	// CREATION DE LA LISTE DES ADHERENTS
    	ServletContext application = getServletConfig().getServletContext();     	
    		// instanciation d'une liste d'adhérent qui sera de portee 'Application'
    	ListeAdherents listAdherents = new ListeAdherents();
    	application.setAttribute("listAdherents", listAdherents);
    	
    	ArrayList<Personne> listPersonne = null;
		try {
			listPersonne = new DaoPersonne().findAll();
		} catch (ExceptionDao e) {
			throw new ServletException(e.getMessage());
		}
    	for (Personne personne : listPersonne) {
    		listAdherents.add(personne);
		}
    	
//    	Personne pers1 = new Personne();
//		try {
//			pers1.setNom("DelaGrandeMarréePlus");
//			pers1.setPrenom("Vyvy");
//			new DaoPersonne().save(pers1);
//			listAdherents.add(pers1);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		try {
//			pers1.setNom("Ryla");
//			pers1.setPrenom("Libou");
//			new DaoPersonne().save(pers1);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		
//    	Personne pers2 = new Personne();
//    	Personne pers3 = new Personne();
//		try {
//			pers1.setNom("DelaGrandeMarrée");
//			pers1.setPrenom("Vyvy");
//			pers2.setNom("DeLaTourEiffel");
//			pers2.setPrenom("Aghate");
//			pers3.setNom("DeLaMarcheEnAvant");
//			pers3.setPrenom("Myrtille");
//			listAdherents.add(pers1);
//			listAdherents.add(pers2);
//			listAdherents.add(pers3);	
//		} catch (Exception e) {			
//			e.printStackTrace();
//		}
	}  
    
    @Override
    public void destroy() {
    	super.destroy();
    	try {	
	        if (connection != null) { connection.close();}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
	// METHODES
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response );
	}
	
	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 HttpSession session = request.getSession(); 
		 if (session.getAttribute("compteurPage") == null) {
	            session.setAttribute("compteurPage", 0);
	   	 }
		
		// ON RECUPERE LE NOM DE LA PAGE PRECISEE DANS L'URL.
			// Avant ! : String cmd = request.getParameter("cmd");	
		String cmd = request.getRequestURI();	 // Maintenant !
		cmd = cmd.substring(17); // /ecoleDeMusique   // Maintenant !
		// ON RECUPERE DANS LA MAP INITIALISEE DANS LE CONSTRUCTEUR
		//  L'INSTANCE DU CONTROLLEUR ASSOCIE A LA PAGE.
		ICommand com = (ICommand)commands.get(cmd);
		// ON LANCE LA METHODE EXECUTE() SUR LE CONTROLLEUR.
		// LE CONTROLLEUR FAIT SON TRAVAIL PUIS
		//  RETOURNE LE NOM DE LA PAGE JSP ASSOCIEE A CE CONTROLLER.
		String urlSuite;
		try {
			urlSuite = com.execute(request, response);
		} catch (Exception e) {
			urlSuite = "erreur.jsp";
			e.printStackTrace();
		}		
		// ON APPELLE LA JSP QUI A POUR NOM LA VALEUR CONTENUS DANS LA STRING 'urlSuite'.
		request.getRequestDispatcher("WEB-INF/JSP/" + urlSuite).forward(request, response);		
	}
}




//// ON RECUPERE LE NOM DE LA PAGE PRECISEE DANS L'URL
//String cmd = request.getParameter("cmd");		
//// ON RECUPERE DANS LA MAP INITIALISEE DANS LE CONSTRUCTEUR
////  L'INSTANCE DU CONTROLLEUR ASSOCIE A LA PAGE.
//ICommand com = (ICommand)commands.get(cmd);
//// ON LANCE LA METHODE EXECUTE() SUR LE CONTROLLEUR.
//// LE CONTROLLEUR FAIT SON TRAVAIL PUIS
////  RETOURNE LE NOM DE LA PAGE JSP ASSOCIEE A CE CONTROLLER.
//String urlSuite;
//try {
//	urlSuite = com.execute(request, response);
//} catch (Exception e) {
//	urlSuite = "erreur.jsp";
//	e.printStackTrace();
//}
//// ON APPELLE LA JSP QUI A POUR NOM LA VALEUR CONTENUS DANS LA STRING 'urlSuite'.
//request.getRequestDispatcher("WEB-INF/JSP/" + urlSuite).forward(request, response);

//Map<String, String[]>  a =  request.getParameterMap();
//System.out.println("la");
//for (Map.Entry<String, String[]> entry : a.entrySet()) {
//    System.out.println("ici " + entry.getKey() + " = " + entry.getValue());
//}
// response.getWriter().append("Served at: ").append(request.getContextPath());
//request.getRequestDispatcher("index.jsp").forward(request, response);
