package metier;

import java.util.ArrayList;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import metier.ListeAdherents;
import modele.beans.Personne;

/**
 * Classe qui regroupe les méthodes CRUD pour la gestion des Personnes.
 * @author Fanny
 *
 */
public class CUDPersonne {
	
	private ArrayList<String> errors = new ArrayList<String>();
	
	public ArrayList<String> getErrors() {
		return errors;
	}
	public void setErrors(ArrayList<String> errors) {
		this.errors = errors;
	}

	/**
	 * Methode qui retourne une instance d une personne d apres 
	 *  les informations entrees dans le formulaire de creation
	 * @param request HttpServletRequest
	 * @return Personne personne
	 */
	public Personne creerInstancierEtValiderPersonne( HttpServletRequest request) {	
		
		ServletContext application = request.getServletContext(); 
    	ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents"); 
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");	
		Personne personne = new Personne();	
		
    	ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    	Validator validator = factory.getValidator(); 
		
		try {
			personne.setNom(nom);
			personne.setPrenom(prenom);			
			Set<ConstraintViolation<Personne>> violations = validator.validate(personne);
			for (ConstraintViolation<Personne> constraintViolation : violations) {
				this.errors.add(constraintViolation.getMessage() + "\n");
			}
			if ( ! violations.isEmpty()) {
				return null;
			}else {
				listPersonne.add(personne);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return personne;
	}
	
	/**
	 * Methode qui retourne l instance de la personne modifiee
	 *  depuis le formulaire de modification
	 * @param request
	 * @return
	 */
	public Personne modiferInstancierEtValiderPersonne( HttpServletRequest request) {	
		
		ServletContext application = request.getServletContext(); 
    	ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents"); 
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");	
		String id = request.getParameter("id");	
		int intId = Integer.valueOf(id);

		Personne personneMofifiee = new Personne();
		
    	ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    	Validator validator = factory.getValidator(); 

		for (Personne personne : listPersonne.getListPersonne()) {
			if (intId == personne.getId().intValue() ) {
				try {
					personneMofifiee.setNom(nom);
					personneMofifiee.setPrenom(prenom);
					Set<ConstraintViolation<Personne>> violations = validator.validate(personneMofifiee);
					if ( ! violations.isEmpty()) { // Un ou plusieurs champs ont une valeur incorrecte :
												   //  la modification est rejetee.
						for (ConstraintViolation<Personne> constraintViolation : violations) {
							this.errors.add(constraintViolation.getMessage() + "\n");
						}						
					}else { // Tous les champs ont une valeur correcte : 
							//  la modification est prise en compte.
						personne.setNom(nom);
						personne.setPrenom(prenom);
						personneMofifiee = personne;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}				
			}
		}
		return personneMofifiee;
	}

	/**
	 * Methode gerant la suppression d'une Personne de la liste des adhérents
	 * Retourne true si la Personne a été supprimée
	 * Prend en parametre une requete Http, contenant l'Id de la personne à supprimer
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	public boolean supprimerPersonneListe( HttpServletRequest request) {	
	
		ServletContext application = request.getServletContext(); 
		ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents"); 
		
		String id = request.getParameter("id");	
		int intId = Integer.valueOf(id);
	
		for (Personne personne : listPersonne.getListPersonne()) {
			if (intId == personne.getId().intValue() ) {
				try {
					listPersonne.remove(personne);
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}				
			}
		}
		return false;
	}
}
