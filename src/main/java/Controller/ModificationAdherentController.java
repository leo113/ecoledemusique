package Controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import metier.CUDPersonne;
import metier.ListeAdherents;
import modele.beans.Personne;
import modele.dao.DaoPersonne;
import modele.forms.PersonneForm;

/**
 * Classe gerant la modification d'un adhérent
 * @author Fanny
 */
public class ModificationAdherentController implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// INCREMENTATION DU NOMBRE DE PAGES VISITEES
		HttpSession session = request.getSession(); 		
 		int nbrPages =  (int) session.getAttribute("compteurPage");
 		nbrPages = nbrPages + 1;
 		session.setAttribute("compteurPage", nbrPages); 
		
		// SETTINGS DE LA PAGE JSP
		request.setAttribute("titreh1", "Modification Adhérent");
		request.setAttribute("titreh2", "MODIFIER ADHERENT :");
		request.setAttribute("classCssVisibility", "visible");	
		
		// RECUPERATION DE LA LISTE GLOBALE DES ADHERENTS
		ServletContext application = request.getServletContext(); 
    	ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents");     	
    	request.setAttribute("listePersonne", listPersonne.getListPersonne());	
    	request.setAttribute("modeRwd", "readonly");
    	
    	// SI ON A SELECTIONNER UNE PERSONNE DANS LA LISTE DEROULANTE :
    	//  ALORS ON LA RECHERCHE DANS LA LISTE DES ADHERENTS
    	//  ET ENSUITE ON ENVOIE SON INSTANCE A LA JSP
    	String adherentAModifier =  request.getParameter("adherentAModifier"); 
    	if (adherentAModifier != null) {
    		adherentAModifier =  (adherentAModifier.split(":")[0]).trim();
		}
    	if (adherentAModifier != null ) {  		
    		for (Personne personne : listPersonne.getListPersonne()) {
				if (personne.getId() == Integer.parseInt(adherentAModifier)) {
		    		request.setAttribute("modeRwd", "");
					request.setAttribute("personne", personne);
				}
			}
		}
    	
    	// SI ON A CLICKER SUR LE BOUTON 'MODIFIER' DU FORMULAIRE :
    	//  ALORS LE FORMULAIRE A ETE  VALIDER ET DONC ON MODIFIE LA PERSONNE
    	if ( (request.getParameterMap().containsKey("nom") ) 
    				&& (request.getParameterMap().containsKey("prenom"))) {
    		
    		// VALIDATION DES CHAMPS DU FORMULAIRE : 
    		// POSITIONNEMENT DE DRAPEAUX CSS 
    		//   POUR L'AFFICHAGE DU MESSAGE DE SUCCESS,
    		//    OU D'ECHEC SI DES CHAMPS SONT INCORRECTS.
    		PersonneForm personneForm = new PersonneForm();  
    		personneForm.verifForm(request);  
    		String classCss = personneForm.getClassCss();
    		String message = personneForm.getResult();
    		
    		// MODIFICATION DE L'OBJET 'PERSONNE' :
    		//  SI UNE/DES VALEURS SONT INCORRECTES 
    		//  L'OBJET 'PERSONNE' N EST PAS MIS A JOUR
    		CUDPersonne cudPersonne = new CUDPersonne();
    		Personne personne = cudPersonne.modiferInstancierEtValiderPersonne(request);    
    		
    		// Si la validation des champs du bean a echouee, on 
    		//  met a jour la classe css et le message,  
    		// sinon on met a jour la personne en Bdd
    		if (! cudPersonne.getErrors().isEmpty() ) { 
    			classCss = "danger";
    			message = "";
	    		for (String error : cudPersonne.getErrors()) {
					message = message + error + "<br>";
				}
    		}else { 
    			new DaoPersonne().save(personne);
    		}
    		
    		// ENVOI DE LA PERSONNE A LA JSP
    		request.setAttribute("personne", personne);
    		
    		// ENVOI DES DRAPEAUX CSS POUR L'AFFICHAGE DU MESSAGE D'ECHEC OU DE SUCCESS
    		request.setAttribute("classCss", classCss);
    		request.setAttribute("message", message);
		}    	
		return "modificationEtCreationAdherent.jsp";
	}
}
