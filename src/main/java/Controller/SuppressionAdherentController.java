package Controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import metier.CUDPersonne;
import metier.ListeAdherents;
import modele.beans.Personne;
import modele.dao.DaoPersonne;

/**
 * Classe permettant la suppression d'un adhérent
 * @author Fanny
 *
 */
public class SuppressionAdherentController implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// INCREMENTATION DU NOMBRE DE PAGES VISITEES
		HttpSession session = request.getSession();
 		int nbrPages =  (int) session.getAttribute("compteurPage");
 		nbrPages = nbrPages + 1;
 		session.setAttribute("compteurPage", nbrPages); 

		
		// RECUPERATION DE L'INSTANCE DE LA LISTE DES ADHERENTS GLOBALE
		//  INITIALISEE DANS LE FRONT-CONTROLLEUR
		ServletContext application = request.getServletContext(); 
    	ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents");     	
    	request.setAttribute("listePersonne", listPersonne.getListPersonne());
    	
    	// POSITIONNEMENT DE DRAPEAUX CSS POUR LA GESTION DE L'AFFICHAGE
    	request.setAttribute("visible", "invisible");
    	request.setAttribute("visibleMessageSuccess", "invisible");
    	request.setAttribute("message", "");
    	
    	// SI ON A SELECTIONNER UN ADHERENT DANS LA LISTE DEROULANTE
    	//  ALORS ON ENVOIT L'INSTANCE DE CETTE PERSONNE A LA JSP
    	//String adherentASupprimer =  request.getParameter("adherentASupprimer"); 
    	
    	String adherentASupprimer =  request.getParameter("adherentASupprimer"); 
    	if (adherentASupprimer != null) {
    		adherentASupprimer =  (adherentASupprimer.split(":")[0]).trim();
		}
    	
    	if (adherentASupprimer != null ) {   		
    		for (Personne personne : listPersonne.getListPersonne()) {
				if ( personne.getId() == Integer.parseInt(adherentASupprimer) ) {
					request.setAttribute("personne", personne);
					request.setAttribute("visible", "visible");
					request.setAttribute("message", "");
				}
			}
		}    	
    	
    	// SI ON A CLICKER SUR LE BOUTON 'SUPPRIMER' DU FORMULAIRE :
    	//  ALORS SUPPRESSION DE LA PERSONNE DE LA LISTE DES ADHERENTS.
    	//  POSITIONNEMENT DE DRAPEAUX CSS POUR L'AFFICHAGE DU MESSAGE DE SUCCESS
    	//  ET LA REINITIALISATION DU FORMULAIRE.
    	if ( (request.getParameterMap().containsKey("id")) ) {
			new CUDPersonne().supprimerPersonneListe(request);	
			new DaoPersonne().delete( Integer.parseInt(request.getParameter("id")));
			request.setAttribute("message", "Adhérent supprimé. Liste des adhérents mise à jour.");
			request.setAttribute("visible", "invisible");
			request.setAttribute("visibleMessageSuccess", "visible");
			request.setAttribute("personne", null);
    	}
	return "suppressionAdherent.jsp";
	}
}
