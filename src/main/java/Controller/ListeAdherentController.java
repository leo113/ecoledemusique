package Controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modele.beans.Personne;
import modele.dao.DaoPersonne;

/**
 * Classe gérant la liste des adhérents
 * @author Fanny
 */
public class ListeAdherentController implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// INCREMENTATION DU NOMBRE DE PAGES VISITEES
		HttpSession session = request.getSession(); 		
	 	int nbrPages =  (int) session.getAttribute("compteurPage");
	 	nbrPages = nbrPages + 1;
	 	session.setAttribute("compteurPage", nbrPages); 

		
		// RECUPERATION DE L'INSTANCE DE LA LISTE D'ADHERENTS GLOBALE,
		//  CREER DANS LA METHODE INIT() DU FRONT-CONTROLLEUR.
		// PUIS, ENVOIT A LA JSP.
	 	
//		ServletContext application = request.getServletContext(); 
//    	ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents"); 		
//		request.setAttribute("listePersonnes", listPersonne.getListPersonne());	
	 	
	 	ArrayList<Personne> listePersonnes = new DaoPersonne().findAll();	 	
	 	request.setAttribute("listPersonnes", listePersonnes);	
		return "listeAdherent.jsp";
	}
}
