package Controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import metier.CUDPersonne;
import modele.beans.Personne;
import modele.dao.DaoPersonne;
import modele.forms.PersonneForm;

/**
 * Controlleur gérant la création d'un adhérent
 * @author Fanny
 * 
 */
public class CreationAdherentController implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// INCREMENTATION DU NOMBRE DE PAGES VISITEES
		HttpSession session = request.getSession(); 
 		int nbrPages =  (int) session.getAttribute("compteurPage");
 		nbrPages = nbrPages + 1;
 		session.setAttribute("compteurPage", nbrPages); 
		
				// ***** CREATION D'UN CLIENT *********	
		
		// SETTINGS DE LA PAGE JSP
		request.setAttribute("titreh1", "Création Adhérent");
		request.setAttribute("titreh2", "CREER ADHERENT :");
		request.setAttribute("classCssVisibility", "invisible");
		request.setAttribute("modeRwd", "");
		
		// VALIDATION DES CHAMPS DU FORMULAIRE : 
		// POSITIONNEMENT DE DRAPEAUX CSS 
		//   POUR L'AFFICHAGE DU MESSAGE DE SUCCESS,
		//    OU D'ECHEC SI DES CHAMPS SONT INCORRECTS.
		PersonneForm personneForm = new PersonneForm();			
		personneForm.verifForm(request); 
		String classCss = personneForm.getClassCss();
		String message = personneForm.getResult();
		
		// TENTATIVE DE CREATION DE LA NOUVELLE PERSONNE.
		// SI DES VALEURS PROVENANT DU FORMULAIRE SONT INCORRECTES,
		//  LA PERSONNE N EST PAS INSTANCIEE,
		//  ET ALORS ON RENVOIT VERS LE FORMULAIRE LES VALEURS DEJA RENSEIGNEES.
		Personne personne = null;
		if (personneForm.isValid()) { // Validation au niveau formulaire : ok
			
			// Tentative instanciation et validation de la personne
			CUDPersonne cudPersonne = new CUDPersonne();
			personne = cudPersonne.creerInstancierEtValiderPersonne(request);				
			
			// Check si la validation du bean a reussie
			if ( cudPersonne.getErrors().isEmpty()) { // Validation bean reussie : la personne a ete instancie
				// Creation d un cokie pour lister les dernieres personnes crées depuis 24h
				int rd1 = (int) (Math.random() * 100000) ;
				Cookie cokieNouvelAdherent = new Cookie("adherent" + rd1 , personne.getNom());
				cokieNouvelAdherent.setMaxAge(60*60*24);
				response.addCookie(cokieNouvelAdherent);
				// ON CREER LA PERSONNE EN BDD
				new DaoPersonne().save(personne);
				// ENVOI DE L'INSTANCE DE LA NOUVELLE PERSONNE A LA JSP POUR AFFICHAGE
				request.setAttribute("modeRwd", "readonly");
				request.setAttribute("personne", personne);
			}else {  // La validation bean a echouee : la Personne n a pas ete instancie
				// DES CHAMPS SONT MANQUANTS OU INCORRECT :
				//  ALORS ON MET A JOUR LES DRAPEAUX ET
				//  ON RENVOIT LES CHAMPS DEJA RENSEIGNES
	    		classCss = "danger";
	    		message = "";
	    		for (String error : cudPersonne.getErrors()) {
					message = message + error + "<br>";
				}
				if (( request.getParameterMap().containsKey("nom")) && (request.getParameterMap().containsKey("prenom")))
				{
					personne = new Personne();
					if (! request.getParameter("nom").isEmpty()) {
						personne.setNom(request.getParameter("nom"));					
					}
					if (! request.getParameter("prenom").isEmpty()) {
						personne.setPrenom(request.getParameter("prenom"));
					}
					request.setAttribute("personne", personne);				
				}
			}
		}else {
			// DES CHAMPS SONT MANQUANTS OU INCORRECT :
			//  ALORS ON RENVOIT LES CHAMPS DEJA RENSEIGNES
			if (( request.getParameterMap().containsKey("nom")) && (request.getParameterMap().containsKey("prenom")))
			{
				personne = new Personne();
				if (! request.getParameter("nom").isEmpty()) {
					personne.setNom(request.getParameter("nom"));					
				}
				if (! request.getParameter("prenom").isEmpty()) {
					personne.setPrenom(request.getParameter("prenom"));
				}
				request.setAttribute("personne", personne);				
			}
		}		
		
		// ENVOI DES DRAPEAUX CSS POUR L'AFFICHAGE DU MESSAGE D'ECHEC OU DE SUCCESS
		request.setAttribute("classCss", classCss);
		request.setAttribute("message", message);
		
		return "modificationEtCreationAdherent.jsp";
	}
}
