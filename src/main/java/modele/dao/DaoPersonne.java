package modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Exceptions.ExceptionDao;
import modele.beans.Personne;
import servlet.FrontController;


public class DaoPersonne {
	
	// METHODE CREATE_PERSONNE(PERSONNE) :
	/**
	 * Creer une nouvelle ligne dans la Bdd 'ecole_musique'
	 * Retourne la Personne si l objet a ete inserer dans la Bdd, sinon retourne null.
	 * Si la Personne a ete inserer, l'id creer en Bdd est ajouter a l instance.
	 * @param personne Personne
	 * @return Personne
	 * @throws SQLException
	 * @throws ExceptionDao
	 */
	public Personne createPersonne(Personne personne) 
			throws SQLException, ExceptionDao {	
		
		if (personne == null) {
			throw new ExceptionDao("Dao Exception. Methode createPersonne(personne)\n"
									+ "La valeur de l'objet 'personne' est null.");
		}
		
		// EXTRACTION DE LA VALEURS DE CHACUN DES ATTRIBUTS
		String nom = personne.getNom();
		String prenom = personne.getPrenom();

		// VERIFICATION SI LA PERSONNE EST DEJA PRESENTE EN BDD
			// SI OUI ON ARRETE LE PROCESS
		Integer idPersonneTemp = personne.getId();
		if ((idPersonneTemp != null )  &&   (find(idPersonneTemp) != null) ) {
			return null;
		}		
			// SINON ON CREER LA PERSONNE EN BDD
		Connection connection = FrontController.getConnection();		
		Statement stmtInsert = null;
		ResultSet resulSetAdresse = null;
		ResultSet resulSetClient = null;
		try {
			stmtInsert = connection.createStatement();			
			String strRequete = null ;
			strRequete = 	"INSERT INTO personne VALUES " 
							+ "(" + null + ",\'" + nom + "\',\'" + prenom
							+ "\'" + ")";
		
			stmtInsert.execute(strRequete, Statement.RETURN_GENERATED_KEYS);
			resulSetClient = stmtInsert.getGeneratedKeys();
			
			// INSERTION DES DONNEES RELATIVE A LA PERSONNE DANS LA TABLE 'PERSONNE'
			if (resulSetClient.next()) {
					// On recupere l idPersonne generer automatiquement par la Bdd
					Integer idPersonneGenerated = resulSetClient.getInt(1);	
			        // ON MET A JOUR L'ID_PERSONNE DANS L'OBJET PASSE EN PARAMETRE A LA FONCTION
			        personne.setId(idPersonneGenerated);
					return personne;
					}else{
						throw new ExceptionDao("Dao Exception. Methode createPersonne.\n"
												+ "Echec creation de la personne " + nom +"." );
			}

		} catch (SQLException  e1) {
			e1.printStackTrace();
		}finally {
			try {	
		    	if (resulSetAdresse  != null) { resulSetAdresse.close();}
		    	if (resulSetClient  != null) { resulSetClient.close();}
		        if (stmtInsert != null) { stmtInsert.close();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	// METHODE DELETE(PERSONNE) :
	/**
	 * Methode qui supprime une Personne dans la Bdd 'ecole_musique'.
	 * @param  idPersonne Integer
	 * @return Boolean
	 * @throws ExceptionDao
	 */
	public Boolean delete(Integer idPersonne) throws ExceptionDao  {	
		
		// SI L ID_PERSONNE EST NULL OU NEGATIF, ON RENVOI UNE EXCEPTION
		if (idPersonne == null || idPersonne < 0) {
			throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
									+ "Impossible de supprimer une personne "
									+ "avec un IdPersonne 'Null' ( ou un idPersonne avec une valeur negative)" );
		}
		
		// CONNECTION BDD
		Connection connection = FrontController.getConnection();
		
		// SUPPRESSION DE LA PERSONNE DANS LE TABLE 'PERSONNE'
		String deleteStringPersonne = "delete from personne"
									 + " where id_personne = '" + idPersonne +"'";	
		try {
			PreparedStatement deleteSociete = connection.prepareStatement(deleteStringPersonne); 
			deleteSociete.executeUpdate();
			return true;
		} catch (SQLException e1) {
			throw new ExceptionDao("Dao Exception. Erreur SQL. La suppression de la personne " 
									+  idPersonne + " a echouée.\n"
									+ e1.getMessage() );
		}	
	}
	
	
	// METHODE FIND(ID_PERSONNE) :
	/**
	 * Recherche l existence d une Personne dans la Bdd 'ecole_musique'
	 *  et retourne  la personne si la Personne est trouvé sinon retourne 'null'.
	 * Prend en parametre l id de la personne a rechercher en Bdd.
	 * @param  int idPersonne
	 * @return Personne personne
	 * @throws ExceptionDao
	 */
	public Personne find(int idPersonne) 
			throws ExceptionDao   {	
		
		if (idPersonne <=0 ) {
			throw new ExceptionDao("Dao Exception. Methode find(personne)\n"
									+ "L idPersonne est negatif.");
		}
		
		Connection connection = FrontController.getConnection();			
		Statement stmt = null;
		ResultSet rs  = null;

		try {
		        // CREATION ET EXECUTION DE LA QUERY DE SELECTION DE LA PERSONNE
				String query = null;
		        query = "Select * from personne "
						+ " where id_personne = "
						+  "'" + idPersonne +"'";

				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        if (rs.next()) {	
				        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
		        	 		Integer id  = rs.getInt("id_personne");
				            String nom  = rs.getString("nom");
				            String prenom  = rs.getString("prenom");
				            
				            //INSTANCIATION DE LA PERSONNE. 
				            Personne personne = new Personne();
				            personne.setId(id);
				            personne.setNom(nom);
				            personne.setPrenom(prenom);
				            
				            return personne;
					}	
	    } catch (Exception e ) {
			e.printStackTrace();
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    	} catch (SQLException e) {
				e.printStackTrace();
	    	}
	    }
	   return null;
	}	
	
	// METHODE FIND_ALL() :
	/**
	 * Recherche la liste des personnes presents dans la Bdd 'ecole_musique'.
	 * Retourne une ArrayList<Personne>.
	 * Retourne une liste vide si la table en Bdd est vide ou si un pbe a été rencontré. 
	 * @return ArrayList<Personne> 
	 * @throws ExceptionDao
	 * @throws SQLException
	 */
	public ArrayList<Personne> findAll() throws ExceptionDao {
		
		ArrayList<Personne> listPersonnes = new ArrayList<>();
		Connection connection = FrontController.getConnection();	
		
		Statement stmt = null;
		ResultSet rs  = null;
		
    	try {
			// CREATION ET EXECUTION DE LA QUERY DE SELECTION DES PERSONNES
			String query = 	"select * from personne";	
    		
			stmt =  connection.createStatement();
			rs = stmt.executeQuery(query);
			
			// CREATION DES OBJETS 'PERSONNES' ET ENREGISTREMENT DANS UNE ARRAY_LIST
			while (rs.next()) {
				// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
	            String nomPersonne  = rs.getString("nom");
	            String prenomPersonne  = rs.getString("prenom");
	            Integer idPersonne  = rs.getInt("id_personne");
	            
	            //INSTANCIATION D'UN NOUVEL OBJET 'PERSONNE'. 
	        	Personne personne = new Personne();
	        	personne.setId(idPersonne);
	        	personne.setNom(nomPersonne);
	        	personne.setPrenom(prenomPersonne);
	        	
	        	listPersonnes.add(personne);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
	    	throw new ExceptionDao(	"ExceptionDaoPersonne. Methode findAll().\n"
	    							+ e1.getMessage());
		} finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    	} catch (SQLException e) {
	    		e.printStackTrace();
	    		throw new ExceptionDao(	"ExceptionDaoPersonne. Methode findAll().\n"
						+ e.getMessage());
	    	}
	    }
		return listPersonnes;
	}
	
	// METHODE SAVE(PERSONNE) :
	/**
	 * Methode qui prend un objet de type Personne en parametre,
	 *  et l'enregistre dans la Bdd. (Tables Personne).
	 * Si la personne existe deja en Bdd, met simplement à jour ses attributs.
	 * Si la personne existe pas encore dans la Bdd, alors rajoute une ligne 
	 * 	dans la Bdd pour cette Personne, dans la table 'Personne'.
	 * Retourne l objet Personne si celui ci a ete enregistrer correctement en Bdd.
	 * Retourne null si un pbe a été rencontré ou si la Personne n a pas de IdPersonne.
	 * @param personne Personne
	 * @return Personne
	 * @throws ExceptionDao
	 */
	public Personne save(Personne personne) throws ExceptionDao {
		
		Connection connection = FrontController.getConnection();	
		
		// SI L OBJET PERSONNE VAUT 'NULL', ON RENVOIT UNE EXCEPTION
		if (personne == null) {
			throw new ExceptionDao("Dao Exception. Methode DaoPersonne.save.\n"
									+ "La Personne ne peut pas avoir une valeur egale à Null." );
		}
		
		// EXTRACTION DES VALEURS DE L OBJET 'PERSONNE' PASSER EN PARAMETRE
		String nomPersonne = personne.getNom();
		String prenomPersonne = personne.getPrenom();
		Integer idPersonne = personne.getId();
	
		// SI LA PERSONNE EXISTE PAS DANS LA BDD, ON LA CREER
		Integer idPersonneTemp = personne.getId();

		if (idPersonne == null) {
			try {
				if (this.createPersonne(personne) != null) {
					 return personne;
				}
			} catch (SQLException | ExceptionDao e) {
				e.printStackTrace();
			}
		}

		if (  ( idPersonne != null)  &&    (find(idPersonneTemp) == null) ) {				
			 try {
				if (this.createPersonne(personne) != null) {
					 return personne;
				}else {
					throw new ExceptionDao(	"Exception Dao. DaoSociete.save()\n" 
											+ "Erreur creation Societe." );
				}
			} catch (SQLException | ExceptionDao e) {
				e.printStackTrace();
			} 
		}
		
		// SI LA PERSONNE EXISTE, ON MODIFIE SES VALEURS DANS LA BDD
		PreparedStatement updatePersonne = null;
		String updateString ;
		
	    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE PERSONNE
		updateString = 	"update personne set "
						+ "personne.nom = ? ,"
						+ "personne.prenom = ?"
						+ "where personne.id_personne = " + "'" + idPersonne + "'";   
		try {
			updatePersonne = connection.prepareStatement(updateString);
			updatePersonne.setString(1,nomPersonne); 
			updatePersonne.setString(2,prenomPersonne); 
			updatePersonne.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}  
        return personne;
	}
	
	
	
	
	
	
	
//	/**
//	 * 
//	 * @param request
//	 * @return
//	 */
//	public Personne creerPersonne( HttpServletRequest request) {	
//		
//		ServletContext application = request.getServletContext(); 
//    	ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents"); 
//		
//		String nom = request.getParameter("nom");
//		String prenom = request.getParameter("prenom");	
//		Personne personne = new Personne();	
//		
//		try {
//			personne.setNom(nom);
//			personne.setPrenom(prenom);
//			listPersonne.add(personne);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return personne;
//	}
	
//	/**
//	 * 
//	 * @param request
//	 * @return
//	 */
//	public Personne modiferPersonne( HttpServletRequest request) {	
//		
//		ServletContext application = request.getServletContext(); 
//    	ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents"); 
//		
//		String nom = request.getParameter("nom");
//		String prenom = request.getParameter("prenom");	
//		String id = request.getParameter("id");	
//		int intId = Integer.valueOf(id);
//
//		Personne personneMofifiee = null;
//
//		for (Personne personne : listPersonne.getListPersonne()) {
//			if (intId == personne.getId().intValue() ) {
//				try {
//					personne.setNom(nom);
//					personne.setPrenom(prenom);
//					personneMofifiee = personne;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}				
//			}
//		}
//		return personneMofifiee;
//	}

//	/**
//	 * Methode gerant la suppression d'une Personne de la liste des adhérents
//	 * Retourne true si la Personne a été supprimée
//	 * Prend en parametre une requete Http, contenant l'Id de la personne à supprimer
//	 * @param request HttpServletRequest
//	 * @return boolean
//	 */
//	public boolean supprimerPersonne( HttpServletRequest request) {	
//	
//		ServletContext application = request.getServletContext(); 
//		ListeAdherents listPersonne= (ListeAdherents) application.getAttribute("listAdherents"); 
//		
//		String id = request.getParameter("id");	
//		int intId = Integer.valueOf(id);
//	
//		for (Personne personne : listPersonne.getListPersonne()) {
//			if (intId == personne.getId().intValue() ) {
//				try {
//					listPersonne.remove(personne);
//					return true;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}				
//			}
//		}
//		return false;
//	}
}
