package modele.beans;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Classe gestion des Personnes
 * @author Fanny
 *
 */
public class Personne {
	
	// ATTRIBUTS D'INSTANCE
	private Integer id ;
	
	@Length(min = 10 , message = "La longueur du nom est de 10 caracteres minimum.")
	@Length(max = 25 , message = "La longueur du nom ne doit pas depasser 25 caracteres.")
	@NotEmpty(message = "Le nom doit avoir une valeur.")
	@NotNull(message = "Le nom ne peut pas etre null.")
	private String nom;
	
	@Length(min = 10 , message = "La longueur du prenom est de 10 caracteres minimum.")
	@Length(max = 25 , message = "La longueur du prenom ne doit pas depasser 25 caracteres.")
	@NotEmpty(message = "Le prenom doit avoir une valeur.")
	@NotNull(message = "Le prenom ne peut pas etre null.")
	private String prenom;	
	
	// GETTERS ET SETTERS
	// Id
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	// Nom
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) throws Exception {
		this.nom = nom;
	}
	// Prenom
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) throws Exception {
		this.prenom = prenom;
	}
	
	@Override
	public String toString() {
		return id + " " + prenom + " " + nom;
	}
}
