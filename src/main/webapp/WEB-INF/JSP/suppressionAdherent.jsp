<%@include file="taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/spacelab/bootstrap.min.css" media="all"/>
</head>
<body>
	<%@ include file="header.jsp" %>
	<div class="container">
	
		<h1 class="text-center m-2 p-2">Suppression Adh�rent</h1>
		
		<!-- MESSAGE DE ERREUR/SUCCESS -->
	 	<div class="alert alert-dismissible alert-success ${visibleMessageSuccess}">
	 		 <button type="button" class="close ${visibleMessageSuccess}" data-dismiss="alert">&times;</button>
	  		 <a href="#" class="alert-link ${visibleMessageSuccess}">${message}</a> 
		</div><br>
		
	 	<!-- FORMULAIRE DE SELECTION D'UN ADHERENT -->
		 <form method="post" action="suppressionAdherent" >
		  <div class="form-group">
		    <label for="exampleFormControlSelect1" class="font-weight-bold font-italic">S�lectionner l'adh�rent � supprimer :</label>
		    <select class="form-control" id="exampleFormControlSelect1" name="adherentASupprimer">
		    	<c:forEach items="${listePersonne}" var="news">
		     		 <option > <c:out value="${news['id']}"></c:out> :<c:out value="${news['nom']}"></c:out>  </option>		     
		        </c:forEach>
		    </select>
		  </div>
			  <input type="submit" value="Valider"> 
		</form> <br><br><br>
		
		<!-- FORMULAIRE DE SUPPRESSION D'UN ADHERENT -->
		<h2 class="m-2">SUPPRIMER L'ADHERENT :</h2>
		<form method="post" action="" >
		  <div class="form-group">
		    <label for="nom">Nom</label>
		    <input readonly value="${personne.nom}"  type="text" class="form-control" id="nom" name="nom" aria-describedby="">
		    <small id="emailHelp" class="form-text text-muted">Nous ne partagerons jamais votre nom avec un tiers.</small>
		  </div>
		  <div class="form-group">
		    <label for="prenom">Prenom</label>
		    <input readonly value="${personne.prenom}" type="text" class="form-control" id="prenom" name="prenom">
		  </div>
		  <div class="form-group">
		    <input value="${personne.id}" type="hidden" class="form-control" id="id" name="id">
		  </div>
		  <div class="form-group">
		    <label class="${visible} font-weight-bold text-danger">Voulez vous vraiment supprimer l'adh�rent ${personne.nom} ?</label><br>
		  	<input class="${visible}  btn btn-danger" type="submit" value="Supprimer" > 
		  </div>
		</form>	

		<%@ include file="footer.jsp" %>
	</div>
</body>
</html>